const enum Direction {
    North = 0,
    East = 1,
    South = 2,
    West = 3
}

const directions = [Direction.North, Direction.East, Direction.South, Direction.West];

class Maze {
    protected data: Uint8Array;
    private walls: number[];
    constructor(private width = 10, private height = 10) {
        this.data = new Uint8Array(width * height);
    }
    public size(): [number, number] {
        return [this.width, this.height];
    }
    public hasEdge(w: number, h: number, dir: Direction): boolean {
        return this.internalHasEdge(h * this.width + w, dir);
    }
    protected internalHasEdge(cell: number, dir: Direction): boolean {
        return (this.data[cell] & (1 << dir)) == 0
    }
    protected getNeighbor(i: number, dir: Direction): number {
        switch (dir) {
            case Direction.North:
                var cell = i - this.width;
                return cell >= 0 ? cell : -1;
            case Direction.East:
                var cell = i + 1;
                return cell % this.width != 0 ? cell : -1;
            case Direction.South:
                var cell = i + this.width;
                return cell < this.data.length ? cell : -1;
            case Direction.West:
                return i % this.width != 0 ? i - 1 : -1;
        }
        return -1;
    }
    protected getUnmarkedNeighbor(i: number, dir: Direction) {
        var n = this.getNeighbor(i, dir);
        return this.marked(n) ? -1 : n;
    }
    protected getNeighbors(i: number, marked: boolean): [Direction, number][] {
        var ret: [Direction, number][] = [];
        for (let dir of directions) {
            var n = marked ? this.getNeighbor(i, dir) : this.getUnmarkedNeighbor(i, dir);
            if (n != -1) ret.push([dir, n]);
        }
        return ret;
    }
    protected removeEdge(cell: number, dir: Direction): boolean {
        if (!this.internalHasEdge(cell, dir)) {
            return false;
        }
        this.data[cell] |= 1 << dir;
        cell = this.getNeighbor(cell, dir);
        this.data[cell] |= 1 << ((dir + 2) % 4);
        return true;
    }
    protected mark(cell: number) {
        this.data[cell] |= (1 << 4);
    }
    protected marked(cell: number): boolean {
        return (this.data[cell] & (1 << 4)) != 0;
    }
    public visit(w: number, h: number) {
        this.data[h * this.width + w] |= (1 << 5);
    }
    public visited(w: number, h: number) {
        return (this.data[h * this.width + w] & (1 << 5)) != 0;
    }
    public canMove(fw: number, fh: number, tw: number, th: number): boolean {
        var src = fh * this.width + fw;
        var target = th * this.width + tw;
        var neighbors = this.getNeighbors(src, true);
        for (let n of neighbors) {
            if (n[1] == target) {
                return !this.internalHasEdge(src, n[0]);
            }
        }
        return false;
    }
}

class DFSMaze extends Maze {
    constructor(width: number, height: number, toRemove: number) {
        super(width, height);

        var stack: number[] = [0];
        while (stack.length > 0) {
            var cell = stack.pop();
            this.mark(cell);
            var n = this.getNeighbors(cell, false);
            if (n.length == 0) continue;
            var r = Math.floor(Math.random()* n.length);
            var [dir, neighbor] = n[r];
            if ((cell % width == 0 && dir == 3) ||
                (cell / height == 0 && dir == 0)) {
                console.log(cell, dir, neighbor);
            }
            this.removeEdge(cell, dir);
            if (n.length > 1) {
                stack.push(cell);
            }
            stack.push(neighbor);
        }

        // Remove random edges, this adds loops to the maze, making it significantly
        // more dificult.
        var tries = toRemove * 4;
        while (toRemove > 0 && tries > 0) {
            var idx = Math.floor(Math.random() * this.data.length);
            var dir = directions[Math.floor(Math.random() * directions.length)];
            if (this.getNeighbor(idx, dir) !== -1 && this.removeEdge(idx, dir)) {
                toRemove--;
            }
            tries--;
        }
    }
}

var m: Maze;
var pre: [number, number] = [0, 0];
var pos: [number, number] = [0, 0];

const padding = 5;
var map: HTMLCanvasElement;
var cellSize: number;
var ctx: CanvasRenderingContext2D;
var width: number;
var height: number;

function drawPath() {
    ctx.fillStyle = "rgb(255, 200, 200)";
    ctx.fillRect(padding + cellSize * pre[0] + 1, padding + cellSize * pre[1] + 1,
        cellSize - 2, cellSize - 2);

    // draw head.
    ctx.fillStyle = "rgb(255, 0, 0)";
    ctx.fillRect(padding + cellSize * pos[0] + 1, padding + cellSize * pos[1] + 1,
        cellSize - 2, cellSize - 2);
}

function drawMap() {
    // draw goal.
    ctx.fillStyle = "rgb(200, 200, 255)";
    ctx.fillRect(padding + (width - 1) * cellSize, padding + (height - 1) * cellSize,
        cellSize, cellSize);

    ctx.beginPath();
    for (var h = 0; h < height; h++) {
        var y = h * cellSize + padding;
        for (var w = 0; w < width; w++) {
            var x = w * cellSize + padding;
            ctx.moveTo(x, y);
            if (m.hasEdge(w, h, Direction.North)) {
                ctx.lineTo(x + cellSize, y);
            } else {
                ctx.moveTo(x + cellSize, y);
            }
            if (m.hasEdge(w, h, Direction.East)) {
                ctx.lineTo(x + cellSize, y + cellSize);
            } else {
                ctx.moveTo(x + cellSize, y + cellSize);
            }
            if (m.hasEdge(w, h, Direction.South)) {
                ctx.lineTo(x, y + cellSize);
            } else {
                ctx.moveTo(x, y + cellSize);
            }
            if (m.hasEdge(w, h, Direction.West)) {
                ctx.lineTo(x, y);
            } else {
                ctx.moveTo(x, y);
            }
        }
    }
    ctx.stroke();
}

function draw() {
    drawPath();
    drawMap();
}

addEventListener("DOMContentLoaded", function () {
    var select = <HTMLSelectElement>document.querySelector("#difficulty");
    var difficulty = select.value;
    var urlparam = /difficulty=(\w+)/.exec(window.location.search);
    if (urlparam && urlparam.length >= 2) {
        select.value = urlparam[1];
        difficulty = urlparam[1];
    }

    switch (difficulty) {
        case 'easy':
            m = new DFSMaze(25, 15, 10);
            break;
        case 'medium':
            m = new DFSMaze(60, 35, 25);
            break;
        case 'hard':
            m = new DFSMaze(137, 75, 50);
            break;
        case 'silly':
            m = new DFSMaze(250, 140, 100);
            break;
        default:
            m = new DFSMaze(60, 35, 25);
            break;
    }
    m.visit(0, 0);

    map = <HTMLCanvasElement>document.querySelector("#map");
    ctx = map.getContext("2d");
    ctx.canvas.width = map.clientWidth;
    ctx.canvas.height = map.clientHeight;
    ctx.strokeStyle = "rgb(0, 0, 0)";
    var pxWidth = map.clientWidth - 2 * padding;
    var pxHeight = map.clientHeight - 2 * padding;
    [width, height] = m.size();
    cellSize = Math.min(Math.floor(pxWidth / width),
        Math.floor(pxHeight / height));
    requestAnimationFrame(draw);

    function registerPoint(x: number, y: number) {
        var w = Math.floor((x - (<HTMLElement>map.offsetParent).offsetLeft) / cellSize);
        var h = Math.floor((y - (<HTMLElement>map.offsetParent).offsetTop) / cellSize);
        if ((w != pos[0] || h != pos[1]) && m.canMove(pos[0], pos[1], w, h)) {
            console.log("hit!", w, h);
            pre = [pos[0], pos[1]];
            pos = [w, h];
            requestAnimationFrame(drawPath);
        }
    }

    map.addEventListener("touchmove", function (e) {
        e.preventDefault();
        var t = e.touches[0];
        registerPoint(t.pageX, t.pageY);
    });
    map.addEventListener("touchstart", function (e) {
        e.preventDefault();
        var t = e.touches[0];
        registerPoint(t.pageX, t.pageY);
    });
    map.addEventListener("mousemove", function (e) {
        registerPoint(e.pageX, e.pageY);
    });
});

addEventListener("keydown", function (e) {
    pre = [pos[0], pos[1]];
    switch (e.key) {
        case 'ArrowUp':
            if (!m.hasEdge(pos[0], pos[1], Direction.North)) {
                pos[1] -= 1;
            }
            break;
        case 'ArrowDown':
            if (!m.hasEdge(pos[0], pos[1], Direction.South)) {
                pos[1] += 1;
            }
            break;
        case 'ArrowLeft':
            if (!m.hasEdge(pos[0], pos[1], Direction.West)) {
                pos[0] -= 1;
            }
            break;
        case 'ArrowRight':
            if (!m.hasEdge(pos[0], pos[1], Direction.East)) {
                pos[0] += 1;
            }
            break;
    }
    m.visit(pos[0], pos[1]);
    requestAnimationFrame(drawPath);
});